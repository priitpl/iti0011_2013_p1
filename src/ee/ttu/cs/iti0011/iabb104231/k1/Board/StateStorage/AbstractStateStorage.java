package ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage;

import java.util.ArrayList;

import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public abstract class AbstractStateStorage {
	
	/**
	 * Retrive the list of free cells.
	 * @return ArrayList<Integer>
	 */
	abstract public ArrayList<Integer> getFreeCells();
	
	/**
	 * Find the winner.
	 * @return AbstractPlayer|null
	 */
	abstract public AbstractPlayer getWinner();
	
	/**
	 * Register player move.
	 * If even AI, but he can use this global register for better moves.
	 * Stores every move, but remembers the player too.
	 * @param player
	 * @param cell
	 */
	abstract public void registerPlayerMove(AbstractPlayer player, Integer cell);
	
	/**
	 * Check, if given cell is occupied and by whom
	 * @param cell
	 * @return
	 */
	abstract public AbstractPlayer occupiedBy(Integer cell);
	
	/**
	 * Checks, if given cell is empty or not.
	 * @param cell
	 * @return
	 */
	public boolean cellIsEmpty(Integer cell)
	{
		return null == occupiedBy(cell);
	}
	
	/**
	 * Try to find good move for given player
	 * @param player
	 * @return
	 */
	public Integer getNextMove(AbstractPlayer player){
		return null;
	}
}
