package ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public class IntegersMapStateStorage extends AbstractStateStorage 
{
	private Map<Integer, AbstractPlayer> moves = new HashMap<Integer, AbstractPlayer>();
	
	public void registerPlayerMove(AbstractPlayer player, Integer cell) 
	{
		moves.put(cell, player);
	}
	
//	public boolean cellIsEmpty(Integer cell)
//	{
//		return !moves.containsKey(cell);
//	}
	
	public AbstractPlayer occupiedBy(Integer cell) 
	{
		if (moves.containsKey(cell)) return moves.get(cell);
		return null;
	}
	
	public ArrayList<Integer> getFreeCells()
	{
		ArrayList<Integer> freeCells = new ArrayList<Integer>();
		
		for(Integer i = 1; i< 10; i++){
			if (!moves.containsKey(i)) {
				freeCells.add(i);
			}
		}
		return freeCells;
	}
	
	public AbstractPlayer getWinner()
	{
		Map<AbstractPlayer, ArrayList<Integer>> playerMoves = new HashMap<AbstractPlayer, ArrayList<Integer>>();
		
		for (Map.Entry<Integer, AbstractPlayer> entry : moves.entrySet()) {
			AbstractPlayer _player = entry.getValue();
			Integer cell = entry.getKey();
			
			if (!playerMoves.containsKey(_player)) playerMoves.put(_player, new ArrayList<Integer>());
			playerMoves.get(_player).add(cell);
			playerMoves.put(_player, playerMoves.get(_player));
		}

		for(Map.Entry<AbstractPlayer, ArrayList<Integer>> entry : playerMoves.entrySet()){
			ArrayList<Integer> pm = entry.getValue(); 
			if (pm.contains(1) && pm.contains(2) && pm.contains(3)) return entry.getKey();
			if (pm.contains(4) && pm.contains(5) && pm.contains(6)) return entry.getKey();
			if (pm.contains(7) && pm.contains(8) && pm.contains(9)) return entry.getKey();
			
			if (pm.contains(1) && pm.contains(4) && pm.contains(7)) return entry.getKey();
			if (pm.contains(2) && pm.contains(5) && pm.contains(8)) return entry.getKey();
			if (pm.contains(3) && pm.contains(6) && pm.contains(9)) return entry.getKey();

			if (pm.contains(1) && pm.contains(5) && pm.contains(9)) return entry.getKey();
			if (pm.contains(3) && pm.contains(5) && pm.contains(7)) return entry.getKey();
		}
		
		return null;
	}
}
