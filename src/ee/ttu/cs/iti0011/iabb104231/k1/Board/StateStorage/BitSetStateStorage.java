package ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.HashMap;
import java.util.Map;

import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy.CertainCellsFirstStrategy;

public class BitSetStateStorage extends AbstractStateStorage{
	
	private Map<AbstractPlayer, BitSet> moves = new HashMap<AbstractPlayer, BitSet>();
	private BitSet allMoves = new BitSet();
	
	private CertainCellsFirstStrategy helperStrategy = new CertainCellsFirstStrategy();
	
	private BitSet findPlayerLastMove(AbstractPlayer player, Integer done)
	{
		if (!moves.containsKey(player)) {
			moves.put(player, new BitSet());
		}
		
		BitSet currentPlayerMoves = moves.get(player);
		BitSet firstBest = null;
		
		Integer bestCount = done;
		
		for(BitSet winRow: getWinning())
		{
			BitSet _winRow = (BitSet)winRow.clone();
			BitSet _allRow = (BitSet)winRow.clone();
			_allRow.and(allMoves);
			_winRow.and(currentPlayerMoves);
			if (_winRow.cardinality() >= bestCount && _allRow.cardinality() == bestCount) {
				firstBest = winRow;
				bestCount = _winRow.cardinality();
			}
		}
		
		return firstBest;
	}
	
	public AbstractPlayer occupiedBy(Integer cell) 
	{
		if (!allMoves.get(cell - 1)) return null;
		for(Map.Entry<AbstractPlayer, BitSet> entry: moves.entrySet()){
			if (entry.getValue().get(cell - 1)) return entry.getKey();
		}
		return null;
	}
	
	public ArrayList<Integer> getFreeCells()
	{
		ArrayList<Integer> freeCels = new ArrayList<Integer>();
		
		for(int i = 0; i < 9; i++){
			if (!allMoves.get(i)) {
				freeCels.add(i+1);
			}
		}
		return freeCels;
	}
	
	public void registerPlayerMove(AbstractPlayer player, Integer cell) 
	{
		if (!moves.containsKey(player)) {
			moves.put(player, new BitSet());
		}
		moves.get(player).set(cell - 1);
		allMoves.set(cell-1);
	}
	
	/**
	 * Find the winner.
	 */
	public AbstractPlayer getWinner()
	{
		// Process each player moves.
		for(Map.Entry<AbstractPlayer, BitSet> entry: moves.entrySet()){
			AbstractPlayer player = entry.getKey();
			BitSet bitset = entry.getValue();
			
			// Now parse all possibilities and apply to given player moves.
			// if intersect with winning move, then we have a winner.
			for(BitSet win: getWinning()){
				BitSet _win = (BitSet)win.clone();
				_win.and(bitset);
				if (_win.equals(win)) {
					return player;
				}
			}
			
		}
		
		return null;
	}
	
	private Integer getLastMove(BitSet winSet, BitSet allMoves)
	{
		BitSet _winSet = (BitSet)winSet.clone();
		_winSet.and(allMoves);
		_winSet.xor(winSet);
		
		return _winSet.length();
	}
	
	public Integer getNextMove(AbstractPlayer player)
	{
		if (!moves.containsKey(player)) {
			moves.put(player, new BitSet());
		}
		
		BitSet firstBest = findPlayerLastMove(player, 2);
		
		// if we have only one place to go, then go.
		if (null != firstBest) {
			return getLastMove(firstBest, allMoves);
		}
		
		// check, if opposite player can't win on next move
		for(AbstractPlayer _player: player.getBoard().getPlayers()){
			if (_player.equals(player)) continue;
			BitSet fs = findPlayerLastMove(_player, 2);
			if (null  != fs ){
				return getLastMove(fs, allMoves);
			}
		}
		for(Integer c: helperStrategy.getPrioritezedMoves()){
			if (allMoves.get(c-1)) continue;
			return c;
		}
		return null;
	}
	
	private ArrayList<BitSet> getWinning()
	{
		ArrayList<BitSet> winnings = new ArrayList<BitSet>();
		
		BitSet a1 = new BitSet(); a1.set(0); a1.set(1); a1.set(2); winnings.add(a1);
		BitSet a2 = new BitSet(); a2.set(3); a2.set(4); a2.set(5); winnings.add(a2);
		BitSet a3 = new BitSet(); a3.set(6); a3.set(7); a3.set(8); winnings.add(a3);

		BitSet a4 = new BitSet(); a4.set(0); a4.set(3); a4.set(6); winnings.add(a4);
		BitSet a5 = new BitSet(); a5.set(1); a5.set(4); a5.set(7); winnings.add(a5);
		BitSet a6 = new BitSet(); a6.set(2); a6.set(5); a6.set(8); winnings.add(a6);
		
		BitSet a7 = new BitSet(); a7.set(0); a7.set(4); a7.set(8); winnings.add(a7);
		BitSet a8 = new BitSet(); a8.set(2); a8.set(4); a8.set(6); winnings.add(a8);
		
		return winnings;
	}
	
}
