package ee.ttu.cs.iti0011.iabb104231.k1.Renderer.Console;

public class Row 
{
	public Integer counter = 1;

	public void resetCounter(){
		counter = 1;
	}
	public void render(String x1, String x2, String x3 )
	{
		x1 = x1 == null || x1 == " " ? (counter).toString() : x1;
		counter++;
		x2 = x2 == null || x2 == " " ? (counter).toString() : x2;
		counter++;
		x3 = x3 == null || x3 == " " ? (counter).toString() : x3;
		counter++;
		System.out.printf("| %s | %s | %s |\n", x1, x2, x3) ;
	}
}
