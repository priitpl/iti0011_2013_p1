package ee.ttu.cs.iti0011.iabb104231.k1.Renderer;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public abstract class AbstractRenderer 
{

	private Board board;

	abstract public void render();
	abstract public void renderTheWinner(AbstractPlayer winner);
	
	public void setBoard(Board board) 
	{
		this.board = board;
	}
	
	public Board getBoard()
	{
		return board;
	}

}
