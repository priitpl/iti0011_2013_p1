package ee.ttu.cs.iti0011.iabb104231.k1.Renderer;

import java.util.HashMap;
import java.util.Map;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;
import ee.ttu.cs.iti0011.iabb104231.k1.Renderer.Console.LineSeparator;
import ee.ttu.cs.iti0011.iabb104231.k1.Renderer.Console.Row;

public class ConsoleRenderer extends AbstractRenderer
{
	private Map<AbstractPlayer, String> playerRender = new HashMap<AbstractPlayer, String>();
	
	public void setPlayerRenderingString(AbstractPlayer player, String string)
	{
		playerRender.put(player, string);
	}
	
	public String getPlayerRenderingString(AbstractPlayer player)
	{
		if (playerRender.containsKey(player)) {
			return playerRender.get(player);
		}
		return " ";
	}
	
	private String cell(AbstractPlayer player)
	{
		if (player == null) {
			return " ";
		}
		return getPlayerRenderingString(player);
	}
	
	public void renderTheWinner(AbstractPlayer winner)
	{
		try{
			getBoard().output("End of the game. And the Winner is:");
			Thread.sleep(1500);
			getBoard().output("....");
			Thread.sleep(1000);
			getBoard().output("....");
			Thread.sleep(500);
			getBoard().output("....");
			Thread.sleep(500);
			if (null != winner) {
				getBoard().output("The Winner is: " + winner.getName());
			} else {
				getBoard().output("Hm ... no winner??? No winner for now.");
			}
			
		} catch(Exception e){
			
		}
		
	}
	
	public void render()
	{
		Board b = getBoard();
		LineSeparator l = new LineSeparator();
		Row r = new Row();
		
		l.render();
		r.render(cell(b.occupiedBy(1)), cell(b.occupiedBy(2)), cell(b.occupiedBy(3)));
		l.render();
		r.render(cell(b.occupiedBy(4)), cell(b.occupiedBy(5)), cell(b.occupiedBy(6)));
		l.render();
		r.render(cell(b.occupiedBy(7)), cell(b.occupiedBy(8)), cell(b.occupiedBy(9)));
		l.render();
	}
	
}
