package ee.ttu.cs.iti0011.iabb104231.k1.Exceptions;

public class NumberOutOfRange extends Exception
{
	static final long serialVersionUID = new Long(11111113);

	public NumberOutOfRange(){
		super("You entered wrong number. Alloed in range between 1-9");
	}
}
