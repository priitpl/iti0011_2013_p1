package ee.ttu.cs.iti0011.iabb104231.k1.Exceptions;

import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public class PlayerCantMoveTwice extends Exception 
{
	static final long serialVersionUID = new Long(11111115);
	
	public PlayerCantMoveTwice(AbstractPlayer player){
		super("You cant move twice, " + player.getName() + ". Relax.");
	}
}
