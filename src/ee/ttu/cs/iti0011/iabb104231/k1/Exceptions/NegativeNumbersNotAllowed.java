package ee.ttu.cs.iti0011.iabb104231.k1.Exceptions;

public class NegativeNumbersNotAllowed extends Exception{

	static final long serialVersionUID = new Long(11111111);
	
	public NegativeNumbersNotAllowed(){
		super("Negative number not allowed");
	}
}
