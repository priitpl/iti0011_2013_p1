package ee.ttu.cs.iti0011.iabb104231.k1.Exceptions;

import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public class PlayerNotSet extends Exception
{
	static final long serialVersionUID = new Long(11111116);
	
	//Parameterless Constructor
//    public PlayerNotSet() {}
	public PlayerNotSet(AbstractPlayer player)
	{
		super("Given player doesnt entered the board: " + player.getName());
	}

    //Constructor that accepts a message
    public PlayerNotSet(String message)
    {
       super(message);
    }
}