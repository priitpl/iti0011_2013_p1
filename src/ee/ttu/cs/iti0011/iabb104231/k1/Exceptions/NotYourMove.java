package ee.ttu.cs.iti0011.iabb104231.k1.Exceptions;

import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public class NotYourMove extends Exception{
	
	static final long serialVersionUID = new Long(11111112);
	
	public NotYourMove(AbstractPlayer player)
	{
		super("It's not your move, player " + player.getName());
	}

}
