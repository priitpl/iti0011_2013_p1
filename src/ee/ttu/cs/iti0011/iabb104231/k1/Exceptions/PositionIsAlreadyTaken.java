package ee.ttu.cs.iti0011.iabb104231.k1.Exceptions;

public class PositionIsAlreadyTaken extends Exception
{
	static final long serialVersionUID = new Long(11111118);
	
	public PositionIsAlreadyTaken()
	{
		super("Sorry, but position is already taken. Make another move");
	}

    //Constructor that accepts a message
    public PositionIsAlreadyTaken(String message)
    {
       super(message);
    }
}
