package ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.AbstractStateStorage;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public abstract class AbstractAIStrategy{

	private Board board;
	
	private AbstractPlayer player;
	
	private AbstractStateStorage storage;

	abstract public Integer getNextMove(AbstractPlayer player);
	
	
	public Integer getNextMove()
	{
		return getNextMove(getPlayer());
	}
	
	public AbstractAIStrategy(AbstractStateStorage storage)
	{
		setStorage(storage);
	}
	
	
	public AbstractStateStorage getStorage() {
		return storage;
	}

	public void setStorage(AbstractStateStorage storage) {
		this.storage = storage;
	}

	public AbstractPlayer getPlayer() {
		return player;
	}

	public void setPlayer(AbstractPlayer player) {
		this.player = player;
	}

	/**
	 * Set board
	 * @param board
	 */
	public void setBoard(Board board){
		this.board = board;
	}
	
	/**
	 * Get Board
	 */
	public Board getBoard()
	{
		return board;
	}
}
