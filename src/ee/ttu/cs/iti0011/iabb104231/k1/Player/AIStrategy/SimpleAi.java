package ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.BitSetStateStorage;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public class SimpleAi extends AbstractAIStrategy
{

	public SimpleAi()
	{
//		super(new SumOf15StateStorage());
		super(new BitSetStateStorage());
	}
	
	public Integer getNextMove(AbstractPlayer player)
	{
		Board board = getPlayer().getBoard();
		if (board.getOpenCells().size() == 0) return null;
		
		return getStorage().getNextMove(player);
	}
}
