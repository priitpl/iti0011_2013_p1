package ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.BitSetStateStorage;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

/**
 * This is smart AI, which actually must remember the previous games
 * and analyze player moves.
 * @author priitpl
 * @todo implement algorithm
 */
public class SmartAss extends AbstractAIStrategy 
{
	public SmartAss()
	{
		super(new BitSetStateStorage());
	}
	
	public Integer getNextMove(AbstractPlayer player)
	{
		return null;
	}
}
