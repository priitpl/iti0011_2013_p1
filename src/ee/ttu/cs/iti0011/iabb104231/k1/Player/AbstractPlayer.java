package ee.ttu.cs.iti0011.iabb104231.k1.Player;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.AbstractStateStorage;

public abstract class AbstractPlayer
{
	private String name;
	
	private Board board;
	
	private AbstractStateStorage storage;
	
	abstract public Integer yourNextMove();
	
	
	public AbstractStateStorage getStorage() {
		return storage;
	}

	public void setStorage(AbstractStateStorage storage) {
		this.storage = storage;
	}

	public void setBoard(Board board)
	{
		this.board = board;
	}
	
	public Board getBoard()
	{
		return board;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
}
