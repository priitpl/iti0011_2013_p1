package ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy;

import java.util.ArrayList;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.IntegersMapStateStorage;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public class CertainCellsFirstStrategy extends AbstractAIStrategy 
{
	public CertainCellsFirstStrategy()
	{
		super(new IntegersMapStateStorage());
	}
	
	public ArrayList<Integer> getPrioritezedMoves()
	{
		ArrayList<Integer> priority = new ArrayList<Integer>();
		
		// Center move
		priority.add(5);
		
		// Corners
		priority.add(1);
		priority.add(9);
		priority.add(3);
		priority.add(7);
//		for(int i = 1; i < 10; i++) {
//			if ((i % 2) == 1 && i != 5) priority.add(i);
//		}
		// all others
		priority.add(4);
		priority.add(8);
		priority.add(6);
		priority.add(2);
//		for(int i = 1; i < 10; i++) {
//			if ((i % 2) == 0 ) priority.add(i);
//		}
		return priority;
	}
	
	public Integer getNextMove(AbstractPlayer player)
	{
		Board board = getPlayer().getBoard();
		
		if (board.getOpenCells().size() == 0) return null;
		
		ArrayList<Integer> emptyCells = board.getOpenCells();

		
		for (Integer i : getPrioritezedMoves()) {
			if (emptyCells.contains(i)) {
				return i;
			}
		}
			
		return null;
	}
}
