package ee.ttu.cs.iti0011.iabb104231.k1.Player;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.AbstractStateStorage;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy.AbstractAIStrategy;

public class AIPlayer extends AbstractPlayer
{
	private AbstractAIStrategy strategy;
	
	public AIPlayer(AbstractAIStrategy strategy)
	{
		super();
		setStrategy(strategy);
	}
	
	public AIPlayer(AbstractAIStrategy strategy, String name)
	{
		super();
		setStrategy(strategy);
		setName(name);
	}
	

	public AbstractStateStorage getStorage()
	{
		return getStrategy().getStorage();
	}
	
	
	/**
	 * Set AI strategy
	 * @param strategy
	 */
	public void setStrategy(AbstractAIStrategy strategy) {
		strategy.setPlayer(this);
		this.strategy = strategy;
	}
	
	/**
	 * Get AI strategy
	 * @return
	 */
	public AbstractAIStrategy getStrategy() {
		return strategy;
	}


	public Integer yourNextMove()
	{
		try{
			Thread.sleep(1000);
		} catch(Exception e){
			
		}
		return getStrategy().getNextMove();
	}
}
