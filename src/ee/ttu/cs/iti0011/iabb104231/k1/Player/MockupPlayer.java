package ee.ttu.cs.iti0011.iabb104231.k1.Player;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.IntegersMapStateStorage;

public class MockupPlayer extends AbstractPlayer
{
	public MockupPlayer()
	{
		super();
		setStorage(new IntegersMapStateStorage());
	}
	public Integer yourNextMove()
	{
		return null;
	}
	
	public void makeMove()
	{
		
	}
}
