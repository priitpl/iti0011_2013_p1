package ee.ttu.cs.iti0011.iabb104231.k1.Player;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.IntegersMapStateStorage;

public class HumanPlayer extends AbstractPlayer
{

	public HumanPlayer() {
		super();
		setStorage(new IntegersMapStateStorage());
	}
	
	public HumanPlayer(String name)
	{
		super();
		setName(name);
		setStorage(new IntegersMapStateStorage());
	}
	
	public Integer yourNextMove()
	{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String cellString;
		
		getBoard().output(getName() + ", enter the number of cell where you wish to move");
		
		try {
			cellString = br.readLine();
			return Integer.parseInt(cellString);
			
		} catch (IOException ioe) {
			System.out.println("IO error trying to read your name!");
		}
		
		return null;
	}
}
