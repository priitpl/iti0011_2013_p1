package ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy;

import java.util.ArrayList;
import java.util.Collections;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Board.StateStorage.BitSetStateStorage;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AbstractPlayer;

public class Dumb extends AbstractAIStrategy
{
	public Dumb()
	{
		super(new BitSetStateStorage());
	}
	
	public Integer getNextMove(AbstractPlayer player)
	{
		Board board = getPlayer().getBoard();
		
		if (board.getOpenCells().size() == 0) return null;
		
		ArrayList<Integer> emptyCells = board.getOpenCells();
		
		Collections.shuffle(emptyCells);
		
		return emptyCells.get(0);
	}
}
