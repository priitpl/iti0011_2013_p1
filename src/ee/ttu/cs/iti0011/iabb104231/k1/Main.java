package ee.ttu.cs.iti0011.iabb104231.k1;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AIPlayer;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.HumanPlayer;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy.CertainCellsFirstStrategy;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy.Dumb;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.AIStrategy.SimpleAi;
import ee.ttu.cs.iti0011.iabb104231.k1.Renderer.ConsoleRenderer;

public class Main {

	public static void main(String[] args) 
	{
	
		// Create AI players. Choose AIStrategy.
		// For now we test simple strategy - dumb - random cell picking
		// and CertainCellsFirst strategy - this is for 1 part.
//		AIPlayer ai1 = new AIPlayer(new Dumb(), "AI - Dumbas");
		AIPlayer ai2 = new AIPlayer(new CertainCellsFirstStrategy(), "AI - 2 lisaosa");
//		AIPlayer ai2 = new AIPlayer(new SimpleAi(), "AI - 2 lisaosa");
		AIPlayer ai1 = new AIPlayer(new SimpleAi(), "AI - 1");
//		HumanPlayer ai1 = new HumanPlayer("Priit");
//		HumanPlayer ai2 = new HumanPlayer("Eugeni");
//		AIPlayer ai2 = new AIPlayer(new Dumb(), "AI - 2");
//		AIPlayer ai2 = new AIPlayer(new Dumb(), "AI - 2");

		// Will create board and add those users here.
		Board b = new Board();
		b.addPlayer(ai1);
		b.addPlayer(ai2);
		
		// Here will create renderer - how the user will see the game.
		// For now the console version is implemented.
		ConsoleRenderer c = new ConsoleRenderer();
		
		// will set marks for users - first user will get X
		// the second one will get O
		// We can set what ever you want - only one char allowed
		c.setPlayerRenderingString(ai1, "o");
		c.setPlayerRenderingString(ai2, "-");
		b.setRenderer(c);
		
//		for(int i=1; i<10; i++) {
//			b.output(i + " : " + Long.toString(b.cellToByte(i).toLongArray()[0], 2));
//		}

		// Starts the game...
		// Now workflow is given to the board and it will manage the game.
		b.startTheGame();
	}

}
