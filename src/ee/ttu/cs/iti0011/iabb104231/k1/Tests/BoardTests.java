package ee.ttu.cs.iti0011.iabb104231.k1.Tests;


import org.junit.Test;

import ee.ttu.cs.iti0011.iabb104231.k1.Board.Board;
import ee.ttu.cs.iti0011.iabb104231.k1.Exceptions.NumberOutOfRange;
import ee.ttu.cs.iti0011.iabb104231.k1.Exceptions.PlayerCantMoveTwice;
import ee.ttu.cs.iti0011.iabb104231.k1.Exceptions.PositionIsAlreadyTaken;
import ee.ttu.cs.iti0011.iabb104231.k1.Player.MockupPlayer;

public class BoardTests {

	@Test(expected = PlayerCantMoveTwice.class)  
	public void oneUserCantMoveTwiceInARow() throws PlayerCantMoveTwice, Exception
	{
		MockupPlayer u1 = new MockupPlayer();
		MockupPlayer u2 = new MockupPlayer();
		u1.setName("mockup 1");
		u2.setName("mockup 2");

		Board b = new Board();
		b.addPlayer(u1);
		b.addPlayer(u2);
		
		try{
			b.makeMove(u1, 1);
			b.makeMove(u1, 1);
		} catch(PlayerCantMoveTwice e){
			throw e;
		} catch(Exception j){
			throw j;
		}
	}
	
	@Test(expected = PositionIsAlreadyTaken.class)  
	public void positionIsAlreadtTaken() throws PositionIsAlreadyTaken, Exception
	{
		MockupPlayer u1 = new MockupPlayer();
		MockupPlayer u2 = new MockupPlayer();
		u1.setName("mockup 1");
		u2.setName("mockup 2");

		Board b = new Board();
		b.addPlayer(u1);
		b.addPlayer(u2);
		
		try{
			b.makeMove(u1, 1);
			b.makeMove(u2, 1);
		} catch(PositionIsAlreadyTaken e){
			throw e;
		} catch(Exception j){
			throw j;
		}
	}
	
	@Test(expected = NumberOutOfRange.class)  
	public void cellNumberOutOfRange() throws NumberOutOfRange, Exception
	{
		MockupPlayer u1 = new MockupPlayer();
		u1.setName("mockup 1");

		Board b = new Board();
		b.addPlayer(u1);
		
		try{
			b.makeMove(u1, 11);
		} catch(NumberOutOfRange e){
			throw e;
		} catch(Exception j){
			throw j;
		}
	}
}
